import json
import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY

def get_picture_url(city, state):
    url = "https://api.pexels.com/v1/search"
    search_photo = {"query": [city, state]}
    headers = {"Authorization": PEXELS_API_KEY}
    response = requests.get(url, params=search_photo, headers=headers)

    picture = json.loads(response.text)

    picture_url = {
        # get url of the first photo
        "picture_url": picture["photos"][0]["url"]
    }

    return picture_url

def get_weather_data(city, state):
    url = "http://api.openweathermap.org/geo/1.0/direct"
    params = {
        "q": f"{city},{state},US",
        "limit": 1,
        "appid": OPEN_WEATHER_API_KEY
        }
    response = requests.get(url, params=params)

    geocoding = json.loads(response.text)
    lat, lon = geocoding[0]["lat"], geocoding[0]["lon"]

    url = "https://api.openweathermap.org/data/2.5/weather"
    params = {
        "lat": lat,
        "lon": lon,
        "appid": OPEN_WEATHER_API_KEY
    }
    response = requests.get(url, params=params)

    weather_data = json.loads(response.text)

    temperature_in_kelvin = weather_data["main"]["temp"]
    temperature_in_Fahrenheit = round((temperature_in_kelvin - 273.15) * (9/5) + 32, 2)
    weather = {
            "temp": temperature_in_Fahrenheit,
            "description":weather_data["weather"][0]["description"]
    }
    return weather
